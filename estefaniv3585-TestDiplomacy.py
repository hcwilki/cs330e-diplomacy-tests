from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):

    #-------
    # read
    #-------
    def test_read1(self):
        city = "A London Support C"
        city_read = diplomacy_read(city)
        self.assertEqual(["A", "London", "Support", "C"], city_read)

    def test_read2(self):
        city = "D Houston Hold"
        city_read = diplomacy_read(city)
        self.assertEqual(["D", "Houston", "Hold"], city_read)
    
    def test_read3(self):
        city = "B Paris Move Austin"
        city_read = diplomacy_read(city)
        self.assertEqual(["B", "Paris", "Move", "Austin"], city_read)

    #-------
    # eval
    #-------
    def test_eval1(self):
        battle = diplomacy_eval([["A", "Paris", "Move", "Barcelona"], ["B", "Barcelona", "Move", "Paris"], ["C", "Tokyo", "Hold"]])
        self.assertEqual([["A", "Barcelona"], ["B", "Paris"], ["C", "Tokyo"]], battle)

    def test_eval2(self):
        battle = diplomacy_eval([["A", "Houston", "Hold"], ["B", "NewYork", "Move", "Houston"], ["C", "London", "Support", "A"], ["D", "Amsterdam", "Support", "C"], ["E", "Madrid", "Move", "London"]])
        self.assertEqual([["A", "[dead]"], ["B", "[dead]"], ["C", "London"], ["D", "Amsterdam"], ["E", "[dead]"]], battle)

    def test_eval3(self):
        battle = diplomacy_eval([["A", "Paris", "Move", "London"], ["B", "Berlin", "Move", "London"], ["C", "NewYork", "Move", "London"], ["D", "Tokyo", "Support", "A"], ["E", "London", "Move", "NewYork"]])
        self.assertEqual([["A", "London"], ["B", "[dead]"], ["C", "[dead]"], ["D", "Tokyo"], ["E", "NewYork"]], battle)


    #-------
    # print
    #-------
    def test_print1(self):
        w = StringIO()
        armies = [["A", "[dead]"], ["B", "Madrid"]]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\n")

    def test_print2(self):
        w = StringIO()
        armies = [["A", "Barcelona"], ["B", "London"], ["C", "[dead]"], ["D", "[dead]"]]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Barcelona\nB London\nC [dead]\nD [dead]\n")

    def test_print3(self):
        w = StringIO()
        armies = [["A", "[dead]"], ["B", "[dead]"], ["C", "Austin"]]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Austin\n")
    
    #------
    # solve
    #------
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB NewYork Move Paris\nC Paris Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Paris\nC [dead]\n")

    def test_solve2(self):
        r = StringIO("A Madrid Move Houston\nB NewYork Support A\nC Houston Move Madrid\nD Prague Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB NewYork\nC Madrid\nD [dead]\n")

    def test_solve3(self):
        r = StringIO("A Tokyo Hold\nB Cairo Move Houston\nC Houston Support A\nD Amsterdam Support C\nE Madrid Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Houston\nD Amsterdam\nE [dead]\n")

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1

$ cat TestCollatz.out
"""
