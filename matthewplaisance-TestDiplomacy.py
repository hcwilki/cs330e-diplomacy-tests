#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = ["This is string"]
        res = diplomacy_read(s)
        self.assertEqual(res, ["This is string"])

    def test_read_2(self):
        s = "Bojack"
        res = diplomacy_read(s)
        self.assertEqual(res, ['B', 'o', 'j', 'a', 'c', 'k'])

    def test_read_3(self):
        s = ""
        res = diplomacy_read(s)
        self.assertEqual(res, [])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid'])
        self.assertEqual(v, ['A [Dead]', 'B [Dead]'])

    def test_eval_2(self):
        v = diplomacy_eval([])
        self.assertEqual(v, [])

    def test_eval_3(self):
        res = diplomacy_eval(["A Austin Move Oymyakon"])
        self.assertEqual(res, ["A Oymyakon"])

    def test_eval_4(self):
        res = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid',
                              'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'])
        self.assertEqual(res, ['A [Dead]', 'B [Dead]',
                               'C [Dead]', 'D Paris', 'E Austin'])
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 1)
        self.assertEqual(w.getvalue(), "1\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "hi")
        self.assertEqual(w.getvalue(), "hi\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, "")
        self.assertEqual(w.getvalue(), "\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10 3\n100 200 4\n201 210 5\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10\n100 200\n201 210\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold B Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")  # no new line between them

    def test_solve_3(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")


# ----
# main
# ----
if __name__ == "__main__":  # pragma: no cover
    main()