#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    # Tests 1-3 are inputs are in non-alphabetical order
    def test_solve_1(self):
        r = StringIO("C Madrid Hold\nA London Hold\nB Austin Hold\nD SanFrancisco Hold\nF NewYork Hold\nE Gotham Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Austin\nC Madrid\nD SanFrancisco\nE Gotham\nF NewYork\n")

    def test_solve_2(self):
        r = StringIO("B Gotham Hold\nF Metropolis Move Gotham\nC CentralCity Move Gotham\nY Atlantis Support F\nZ NewYork Move Gotham\nM SanFrancisco Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "B [dead]\nC [dead]\nF [dead]\nM SanFrancisco\nY Atlantis\nZ [dead]\n")

    def test_solve_3(self):
        r = StringIO("F SanAntonio Move Austin\nL FortWorth Move Dallas\nD Austin Hold\nO Waco Support L\nM London Move Waco")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "D [dead]\nF [dead]\nL Dallas\nM [dead]\nO [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Austin Hold\nB Beijing Hold\nC Calgary Move Austin\nD Dublin Move Austin\nE Edinburgh Support A\nF Frankfurt Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Beijing\nC [dead]\nD [dead]\nE Edinburgh\nF Frankfurt\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\n")

    # -----
    # read
    # -----

    def test_read_1(self):
        s = 'A Madrid Hold\n'
        token_list = diplomacy_read(s)
        self.assertEqual(token_list, ['A', 'Madrid', 'Hold'])

    def test_read_2(self):
        s = 'Z Gotham Move Metropolis'
        token_list = diplomacy_read(s)
        self.assertEqual(token_list, ['Z', 'Gotham', 'Move', 'Metropolis'])

    def test_read_3(self):
        s = 'E CentralCity Support A'
        token_list = diplomacy_read(s)
        self.assertEqual(token_list, ['E','CentralCity', 'Support', 'A'])


    # -----
    # print
    # -----
    def test_print_1(self):
        w = StringIO()
        army_dict = {'A': 'A Austin', 'B': 'B Dallas', 'C': 'C FortWorth'}
        diplomacy_print(w, army_dict)
        self.assertEqual(w.getvalue(), 'A AustinB DallasC FortWorth')


    def test_print_2(self):
        w = StringIO()
        army_dict = {'D': 'D Gotham', 'A': 'A CentralCity', 'G': 'G Metropolis', 'E': 'E Madrid'}
        diplomacy_print(w, army_dict)
        self.assertEqual(w.getvalue(), 'A CentralCityD GothamE MadridG Metropolis')


    def test_print_3(self):
        w = StringIO()
        army_dict = {'Z': 'Z Barcelona', 'Y': 'Y Lahore', 'M': 'M Manchester', 'H': 'H Chicago', 'N': 'N Austin'}
        diplomacy_print(w, army_dict)
        self.assertEqual(w.getvalue(), 'H ChicagoM ManchesterN AustinY LahoreZ Barcelona')

    def test_print_4(self):
        w = StringIO()
        army_dict = {'B': 'B Barcelona', 'D': 'D Dallas', 'L': 'L Lahore', 'I': 'I Irvine', 'A': 'A Austin'}
        diplomacy_print(w, army_dict)
        self.assertEqual(w.getvalue(), 'A AustinB BarcelonaD DallasI IrvineL Lahore')

# ----
# main
# ----

#pragma: no cover
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
............
----------------------------------------------------------------------
Ran 12 tests in 0.001s

OK


$ coverage report -m                   >> TestDiplomacy.out


$ cat TestDiplomacy.out
............
----------------------------------------------------------------------
Ran 12 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py         125      0     56      0   100%
TestDiplomacy.py      63      0      2      0   100%
--------------------------------------------------------------
TOTAL                188      0     58      0   100%
"""
